import math
import random
from random import shuffle
"""
Initial input classes
"""
"""Main Input Board players will see"""
import os
print(os.path.abspath(os.curdir))
os.chdir('/home/quang/PycharmProjects/sudoku')
# print(os.path.abspath(os.curdir))
f = open( "InputData.txt", "r" )
lines = []
for line in f:
    lines.append(line)
f.close()

positions = lines[0::2]
vals1 = lines[1::2]
positions.pop()
for i in range(len(positions)):
    positions[i] = positions[i].replace("\n","")
    vals1[i] = vals1[i].replace("\n","")


class Board(object):
    rowIDs = []

    def __init__(self, sqr=[],board=[]):
        self.sqr = sqr
        self.rowList = []
        self.constant = []
        self.board = board
        for i in range(9):
            row = []
            for j in range(9):
                row.append([])
            self.board.append(row)
    """prints board"""
    def show(self):
        # row1 = [[],[],[],[],[],[],[],[],[]]

        # print("You have created a new sudoku board")
        for i in self.board:
            print(i)
    """lists the rows user will edit on board 1-9"""
    def rowModify(self):

        inputNum = False
        while inputNum is False:
            r = input("Enter which rows you will add or modify: ")
            r = r.replace(",", "")
            r = r.replace(" ", "")
            for num in r:
                self.rowList.append(int(num) - 1)
            if any(num > 8 or num < 0 for num in self.rowList):
                inputNum = False
                self.rowList = []
                print("Enter number is outside 1-9 bound, re-enter number: ")
            else:
                inputNum = True
    """fill in values and their positions in each of the chosen rows"""
    def initialize(self,rowID,positions,vals):
        temp_row = [0,0,0,0,0,0,0,0,0]
        n = 0
        for pos in positions:
            temp_row[pos] = vals[n]
            n += 1
        self.board[rowID] = temp_row
        self.sqr[rowID].row = temp_row

    def cellIdConst(self,rowID,colID):
        cellID = omg.FindCellId(rowID,colID)
        self.constant.append(cellID)


"""Object rows players will input the values and their positions"""
class Row(object):

    def __init__(self,id):
        # self.row = []
        self.id = id
        self.posList = []
        self.vals = []
        """initiate user to input values,position, etc"""
    def rowInput(self,board):
        print("rowID",self.id)
        for num in positions[self.id]:
            self.posList.append(int(num) - 1)
        print(self.posList)
        for val in vals1[self.id]:
            self.vals.append(int(val))
            print(self.vals)
        board.initialize(self.id,self.posList,self.vals)
        #position = a list of positions [1-9], an array e.g. [1,3,5]
        # while modifyComp is False:
        #     inputNum = False
        #     while inputNum is False:
        #         position = input("Enter the positions for row %s you would like to add/modify the value(1-9: "%self.id)
        #         rowC += 1
        #         temp = position.replace(",","")
        #         temp = temp.replace(" ","")
        #         for num in temp:
        #             self.posList.append(int(num) -1)
        #             if any(num > 8 or num < 0 for num in self.posList):
        #                 inputNum = False
        #                 self.poslist = []
        #                 print("Enter number is outside 1-9 bound, re-enter number")
        #             else:
        #                 inputNum = True
        #         n = len(self.posList)
        #
        #         #
        #         value = input("Enter the numbers you want for these positions %s : " %(self.posList))
        #         temp = value.replace(",", "")
        #         temp = temp.replace(" ", "")
        #         for val in temp:
        #             self.vals.append(int(val))
        #         userComp = input("Are you done?:(Y/N) ")
        #         if userComp.lower() == "y":
        #             modifyComp = True
        #         else:
        #             modifyComp = False
        #
        #     board.initialize(self.id, self.posList, self.vals)

"""***********************************************"""
"""
Program logic to solve sudoku
"""
"""***********************************************"""
class Xlist(object): #used to create new sudoku boards

    def __init__(self,x1,x2,x3,x4,x5,x6,x7,x8,x9,re = []):
        self.x = [x1,x2,x3,x4,x5,x6,x7,x8,x9]
        self.re = re

class Col(object):

    def __init__(self,attempt = []):
        self.ref = random.sample(range(1,10),9)

class X(object):
    def __init__(self,id):
        self.col = []
        self.id = id


# class Y(object):
#     def __init__(self,id):
#         self.row = []
#         self.id = id



    """
    Issues that need to be resolved
    (1) Need to update all box related variables
    (2) need to finish switch function tomorrow
    """
def switch(col, numAvail,rowID,row):
    switchDone = False
    posConst = b1.sqr[rowID].posList
    n = col
    print("switch internal colid",n, "position constraints",posConst)
    boxID_col = box1.findBoxID(rowID,col)
    while switchDone is False and n >= 0:
        n -= 1
        while any(n == i for i in posConst):
            n -= 1
        else:
            pass
        boxID_n = box1.findBoxID(rowID,n)
        isSameBox = boxID_col == boxID_n

        if isSameBox is False:
            print("isSameBox N", n)
            switchVal = row[n]

            for i in numAvail:

                row[n] = i
                row[col] = switchVal
                print("Each num avail", i, "row", row)
                """ Check if row have duplicates after switch"""
                temp = []
                for i in row:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                rowCheck = len(set(temp)) == len(temp)
                """ Check if either columns have duplicates after switch"""
                n_col = colUpdate(row, rowID, n)
                temp = []
                for i in n_col:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                n_colCheck = len(set(temp)) == len(temp)
                print(temp, "n_col", n_colCheck)
                colCheck = colUpdate(row, rowID, col)
                temp = []
                for i in colCheck:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                col_colCheck = len(set(temp)) == len(temp)
                print(temp, "col_col", col_colCheck)
                """ Check if box has any duplicates after switch """
                n_box = boxUpdate(boxID_n, rowID, n, row)
                temp = []
                for i in n_box:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                n_boxCheck = len(set(temp)) == len(temp)
                print(temp, "n_box", n_boxCheck)
                c_box = boxUpdate(boxID_col, rowID, col, row)
                temp = []
                for i in c_box:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                col_boxCheck = len(set(temp)) == len(temp)
                print(temp, "col_box", col_boxCheck)
                print("row", row, rowCheck, "coln", n_colCheck, "colcol", col_colCheck, "n_box",n_colCheck, "c_box",col_boxCheck)

                if rowCheck is False or col_colCheck is False or n_colCheck is False or col_boxCheck is False or n_boxCheck is False:
                    row[n] = switchVal
                    row[col] = 0
                    # box1.sqr[boxID_n] = [switchVal if x == i else x for x in box1.sqr[boxID_n]]
                    # box1.sqr[boxID_col].pop()
                    # lst_of_X[n].col.pop()
                    # a = lst_of_X[col].col.pop()
                    # lst_of_X[n].col.append(a)

                else:
                    print("good row -",row)
                    break

            if rowCheck is False or col_boxCheck is False or n_boxCheck is False or n_colCheck is False or col_colCheck is False:
                pass
            else:
                switchDone = True

        else:
            print("SameBox Y","n", n)
            print("row[n-1]", row[n - 1])
            switchVal = row[n]
            for i in numAvail:
                print("Each num avail", i, "row",row)

                row[n] = i
                # box1.sqr[boxID_n] = [i if x==switchVal else x for x in box1.sqr[boxID_n]]
                # lst_of_X[n].col = [i if x == switchVal else x for x in lst_of_X[n].col]
                row[col] = switchVal
                # box1.sqr[boxID_col].append(switchVal)
                # lst_of_X[col].col.append(switchVal)
                """ Check if row have duplicates after switch"""
                temp = []
                for i in row:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                rowCheck = len(set(temp)) == len(temp)
                """ Check if either columns have duplicates after switch"""
                n_col = colUpdate(row, rowID, n)
                temp = []
                for i in n_col:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                n_colCheck = len(set(temp)) == len(temp)
                print(temp, "n_col", n_colCheck)
                colCheck = colUpdate(row, rowID, col)
                temp = []
                for i in colCheck:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                col_colCheck = len(set(temp)) == len(temp)
                print(temp, "col_col", col_colCheck)
                """ Check if box has any duplicates after switch """
                n_box = boxUpdate(boxID_n, rowID, n, row)
                temp = []
                for i in n_box:
                    if i != 0:
                        temp.append(i)
                    else:
                        pass
                n_boxCheck = len(set(temp)) == len(temp)
                print(temp, "boxid", boxID_n, n_boxCheck)
                print("row", row, rowCheck, "coln", n_colCheck,"colcol", col_colCheck)
                """ Passes on row if row is good or switches values back if any criteria fails"""
                if rowCheck is False or n_boxCheck is False or n_colCheck is False or col_colCheck is False:
                    row[n] = switchVal
                    row[col] = 0
                    #box1.sqr[boxID_n] = [switchVal if x == i else x for x in box1.sqr[boxID_n]] #need new update function
                    # box1.sqr[boxID_n] = box1.update(boxID_n)
                    # lst_of_X[n].col.pop()
                    # a = lst_of_X[col].col.pop()
                    # lst_of_X[n].col.append(a)
                else:
                    print("good row -",row)
                    break
            if rowCheck is False or n_boxCheck is False or n_colCheck is False or col_colCheck is False:
                pass
            else:
                switchDone = True
    if n< 0:
        print("Error, n < 0")
        for y in b1.board:
            print(y)
    else:
        pass
    return row
"""
generate a list of empty individual cells. Will be use to add attempted numbers. WIll be heavilu used in backtrack method
"""
class SqrRefList(object):
    sqr_ref_list = []
    def __init__(self):
        for i in range(81):
            x = []
            for j in range(1, 10):
                x.append(j)
            self.sqr_ref_list.append(x)
        self.ref_cnt = []

    def FindCellId(self,row,col):
        cellId = row*9 + col
        return cellId
    def FindRowFromCell(self,cellID):
        row = int(cellID/9)
        return row
    def FindColFromCell(self,celID):
        col = celID % 9
        return col
    """reduce downstream possibilities"""
    def removeFutureVal(self,rowID,colID):
        # remove all possibilities from row
        startCell = rowID * 9
        val = b1.board[rowID][colID]
        # print("val to be removed",val)
        for k in range(startCell,startCell+9):
            # print("at positions cell", k)
            try:
                self.sqr_ref_list[k].remove(val)
            except ValueError:
                pass

        #remove all possibilities from column
        for k in range(colID,81,9):
            # print("at position cell,",k)
            try:
                self.sqr_ref_list[k].remove(val)
            except ValueError:
                pass
        #remove all possibilities from box
        boxID = box1.findBoxID(rowID,colID)
        #need all the cell id from a box given a boxID
        r = boxRow(boxID)
        c = boxCol(boxID)

        for i in r:
            for j in c:
                try:
                    cellID = self.FindCellId(i, j)
                    # print("from row",i,"& col", j,"at position cell", cellID)
                    self.sqr_ref_list[cellID].remove(val)
                except ValueError:
                    pass

    def addFutureVal(self,rowID,colID,val):
        cellID = self.FindCellId(rowID,colID)
        # add all possibilities to row
        startCell = rowID * 9
        # print("val to be added", val)
        for k in range(startCell, startCell + 9):
            # print("at positions cell", k)
            if any(i == val for i in self.sqr_ref_list[cellID]):
                pass
            else:
                self.sqr_ref_list[cellID].append(val)

        # add all possibilities to col
        for k in range(colID, 81, 9):
            # print("at position cell,", k)
            if any(i == val for i in self.sqr_ref_list[cellID]):
                pass
            else:
                self.sqr_ref_list[cellID].append(val)

        # add all possibilities to box
        boxID = box1.findBoxID(rowID, colID)
        # need all the cell id from a box given a boxID
        r = boxRow(boxID)
        c = boxCol(boxID)

        for i in r:
            for j in c:
                b_cellID = self.FindCellId(i, j)
                if any(i == val for i in self.sqr_ref_list[b_cellID]):
                    pass
                else:
                    print("from row", i, "& col", j, "at position cell", b_cellID)
                    self.sqr_ref_list[b_cellID].append(val)

    def calcRemainingRefNums(self):
        temp = []
        for i in self.sqr_ref_list:
            if len(i) < 1:
                return
            temp.append(len(i))
        self.ref_cnt = temp



omg = SqrRefList()
cell_attempt_list = []
for i in range(81):
    x = []
    for j in range(1,10):
        x.append([])
    cell_attempt_list.append(x)

"""
3x3 constraint reference list
"""
class Boxes(object):
    def __init__(self):
        self.sqr = [[],[],[],[],[],[],[],[],[]] #used to append numbers as they're added to check for duplicates
        self.check = [[],[],[],[],[],[],[],[],[]] #used to determine if everything is passing: box is either "Good" or "bad"

    def findBoxID(self,row,m):
        if row < 3 and m < 3:
            return 0
        elif row < 3 and m >= 3 and m < 6:
            return 1
        elif row <3 and m >= 6:
            return 2
        elif row >= 3 and row < 6 and m < 3:
            return 3
        elif row >= 3 and row < 6 and m >= 3 and m < 6:
            return 4
        elif row >= 3 and row < 6 and m >= 6:
            return 5
        elif row >= 6 and row < 9 and m < 3:
            return 6
        elif row >= 6 and row < 9 and m >= 3 and m < 6:
            return 7
        elif row >= 6 and row < 9 and m >= 6:
            return 8
        else:
            return 10

    def update(self,id):
        if id < 3:
            row1 = b1.board[0]
            row2 = b1.board[1]
            row3 = b1.board[2]
        elif id >= 3 and id < 6:
            row1 = b1.board[3]
            row2 = b1.board[4]
            row3 = b1.board[5]
        elif id > 6:
            row1 = b1.board[6]
            row2 = b1.board[7]
            row3 = b1.board[8]
        else:
            print("error")
        rows = [row1,row2,row3]

        # print("length",y_len, "rows", rows)
        temp = []
        if id == 0 or id == 3 or id == 6:
            for j in range(3):
                for ll in range(3):
                    temp.append(rows[j][ll])

        elif id == 1 or id == 4 or id == 7:

            for j in range(3):
                for ll in range(3,6):
                    temp.append(rows[j][ll])

        elif id == 2 or id == 5 or id == 8:

            for j in range(3):
                for ll in range(6,9):
                    print("ll",ll)
                    temp.append(rows[j][ll])

                print(temp)
        else:
            pass
        # print("update function executed current box:",self.sqr[id], "updated box:", temp)

        return temp


box1 = Boxes()
#
# y1 = Y(1)
# y2 = Y(2)
# y3 = Y(3)
# y4 = Y(4)
# y5 = Y(4)
# y6 = Y(5)
# y7 = Y(6)
# y8 = Y(7)
# y9 = Y(8)

# lst_of_Y = [y1,y2,y3,y4,y5,y6,y7,y8,y9]

x1 = X(1)
x2 = X(2)
x3 = X(3)
x4 = X(4)
x5 = X(5)
x6 = X(6)
x7 = X(7)
x8 = X(8)
x9 = X(9)

lst_of_X = [x1,x2,x3,x4,x5,x6,x7,x8,x9]

def colUpdate(row,rowID,colID):
    cellVal = row[colID]
    tempCol = []
    for i in range(9):
        if i == rowID:
            tempCol.append(cellVal)
        else:
            tempCol.append(b1.board[i][colID])
    return tempCol



"""
row generations of base row
"""
"""
Read Me
for rowGen function. var m must start at 0 if it is a new empty row. If row has values, then m can start with next empty position.
if row is not empty, m cannot be 0
"""
def newRowGen(row,ref_list,rowID,colID,const): #const is the 4th constrant - values that cannot be modified or moved
    colID_ori = colID
    c_position = const
    try:
        isRowDone = len(set(row)) == 9 and sum(row,0)
    except TypeError:
        isRowDone = False
    print("isRowDone",isRowDone, len(row) )
    triggerSwitch = False
    zz = 0
    while isRowDone is False:

        n = 0
        constCell = False
        #is it a const?
        while constCell is False:
            if any(c == colID for c in const) == True:
                constCell = False
                colID += 1
            else:
                constCell = True

        cell = omg.FindCellId(rowID,colID)
        boxID = box1.findBoxID(rowID, colID)
        print("ref_cnt",omg.ref_cnt,"cell", cell,omg.sqr_ref_list[cell])
        print("first col", colUpdate(row,rowID,0), "last col",colUpdate(row,rowID,8), "cell", cell, ref_list[cell], "cell + 1", cell+1, ref_list[cell+1])
        print("rowID", rowID, "row:", row)
        print("colID", colID, "col", colUpdate(row, rowID, colID))
        print("boxID", boxID, "box", boxUpdate(boxID, rowID, colID, row))

        rowCheck = len(set(row)) == len(row) and any(val != 0 for val in row)

        boxCheck = len(set(box1.sqr[boxID])) == len(box1.sqr[boxID]) and any(val != 0 for val in box1.sqr[boxID])
        #check if cell or row/col combo is a constant
        """input: rowID, col"""
        #add number to row
        rCount = []
        for i in row:
            if i != 0:
                rCount.append(i)
            else:
                pass
        print("rcount",rCount)
        rowCheck = len(set(rCount)) == len(rCount)
        if len(set(rCount)) < 9:
            shuffle(ref_list[cell])
            add = ref_list[cell][n]
            # print("add", add)
            row[colID] = add
            box = boxUpdate(boxID, rowID, colID, row)

            # box1.sqr[boxID].append(add)
            """
            Issue #2 - possible cursor is not aligned to correct cell
            """
            # print("colID", colID,"lst_of_X[colID]", lst_of_X[colID].col)
            # print("before xcol",lst_of_X[0].col,lst_of_X[8].col, "add", add)
            # lst_of_X[colID].col.append(add)
            # numberTry(add, cell, n)
         # moves to the next cell to add to the col

            print("#3 main additiona logic","cell",cell, "row", row, "add", add)
            colID += 1
        else:
            print("length achieved")
            pass

        b_cell = omg.FindCellId(rowID, colID - 1)
        b_boxID = box1.findBoxID(rowID, colID - 1)
        currCol = colUpdate(row,rowID,colID -1)
        #check column

        cCount = []
        for i in currCol:
            if i != 0:
                cCount.append(i)
            else:
                pass
        print(colUpdate(row,rowID,colID - 1),len(set(cCount)),"set",set(cCount))
        colCheck = len(set(cCount)) == len(cCount)
        print("colCheck", colCheck)

        if colCheck is False:
            print(row[colID-1], "removed - col")
            row[colID - 1] = 0
            # box_duplicate = box1.sqr[b_boxID].pop()


            if len(ref_list[cell]) == 0:
                triggerSwitch = True
            else:
                pass
        else:
            iColCheck = True

        # check row
        rCount = []
        for i in row:
            if i != 0:
                rCount.append(i)
            else:
                pass
        rowCheck = len(set(rCount)) == len(rCount)
        if rowCheck is False:
            iRowCheck = False
            print("row - removed", row[colID - 1])
            row[colID - 1] = 0
            # box_duplicate = box1.sqr[b_boxID].pop()

            if len(ref_list[cell]) == 0:
                triggerSwitch = True
            else:
                pass
        else:
            iRowCheck = True

        #Check box
        box = boxUpdate(boxID, rowID, colID - 1,row)
        print()
        bCount = []
        for i in box:
            if i != 0:
                bCount.append(i)
            else:
                pass
        boxCheck = len(set(bCount)) == len(bCount)
        if boxCheck is False:
            iBoxCheck = False
            print(row[colID - 1], "removed - box")
            row[colID - 1] = 0
            # box_duplicate = box1.sqr[b_boxID].pop()
            if len(ref_list[cell]) == 0:
                triggerSwitch = True
            else:
                pass
        else:
            iBoxCheck = True
        """ Checks if number added will cause downstream possibilities to go to zero"""
        if iRowCheck is True and iColCheck is True and iBoxCheck is True:
            omg.removeFutureVal(rowID,colID - 1)
            omg.calcRemainingRefNums()
            print("ref_cnt",omg.ref_cnt)
            t_cell = omg.FindCellId(rowID,colID -1)
            emptyCell = [i for i in range(81) if i not in b1.constant]
            emptyCellLen = [i for i in emptyCell if len(omg.sqr_ref_list[i]) < 1]
            removedPreviousCells = [i for i in emptyCellLen if i > t_cell]
            print("removedPreviousCells length",len(removedPreviousCells), "array",removedPreviousCells)
            if len(removedPreviousCells) > 0:
                if len(ref_list[t_cell]) == 0:
                    triggerSwitch = True
                else:
                    pass
                print("value", row[colID-1],"removed bc current cell", t_cell,"cause downstream issues")
                omg.addFutureVal(rowID,colID - 1,row[colID-1])
                row[colID - 1] = 0
                futureCheck = False
            else:
                futureCheck = True
        zz += 1
        #is row done?
        rCount = []
        for i in row:
            if i != 0:
                rCount.append(i)
            else:
                pass
        rowLength = len(set(rCount)) == 9
        print("bottom rowlength",rowLength)
        rowCheck = len(set(row)) == len(row)

        # boxCheck = len(set(box1.sqr[boxID])) == len(box1.sqr[boxID])
        if rowLength is True:
            print("#1")
            if futureCheck is True:
                print("future check is true")
                if iColCheck is True:
                    print("#2")
                    if iRowCheck is True:
                        print("#3")
                        boxID = box1.findBoxID(rowID, colID_ori)
                        boxGood = 0
                        a = [2, 5, 8]
                        n = 0
                        print("boxID",boxID)
                        for id in range(boxID,boxID + 3):
                            box2 = boxUpdate(id, rowID, a[n], row)
                            print("#3 box", box2)
                            bCount = []
                            for i in box2:
                                if i != 0:
                                    bCount.append(i)
                                else:
                                    pass
                            if len(set(bCount)) == len(bCount):
                                boxGood += 0
                            else:
                                boxGood += 1
                            n += 1
                            print("bCount",bCount, boxGood)
                        if boxGood == 0:
                            print("Row Complete")
                            isRowDone = True
                            return row
                        else:
                            print("Error #1")
                            isRowDone = False
                            colID -= 1
                    else:
                        isRowDone = False
                        colID -= 1
                else:
                    isRowDone = False
                    colID -= 1
            else:
                isRowDone = False
                colID -=1
        else:
            isRowDone = False
            print("trigger switch", triggerSwitch)
            if triggerSwitch is True:
                numAvail = [i for i in [1, 2, 3, 4, 5, 6, 7, 8, 9] if i not in row]
                print("#4", "col", colID,"numAvail", numAvail,"ref_list", ref_list[cell], "cell", cell)
                #run switch function
                # colID -= 1
                print("before switch code - colID ",colID - 1)
                # yID = b1.sqr[rowID]
                row = switch(colID - 1,numAvail, rowID,row)
                colID = len(row) - 1
                print('switch code executed',row, "colID", colID)
            elif colCheck is False:
                colID -= 1
                print("end colcheck")
            elif iRowCheck is False:
                print("end rowcheck")
                colID -= 1
            elif iBoxCheck is False:
                colID -= 1
                print("end boxcheck")
            elif futureCheck is False:
                colID -= 1
                print("end futureCheck")
            else:
                print("Next cell")
                pass


"""***********************************************"""

def initialize_board(board):

    y0 = Row(0)
    y1 = Row(1)
    y2 = Row(2)
    y3 = Row(3)
    y4 = Row(4)
    y5 = Row(5)
    y6 = Row(6)
    y7 = Row(7)
    y8 = Row(8)
    board.sqr = [y0,y1,y2,y3,y4,y5,y6,y7,y8]

def sudoku_input():

    b1.show()
    b1.rowModify()
    print("rowList", b1.rowList)
    rows = b1.rowList
    for i in range(9):
        if i in rows:
            b1.sqr[i].rowInput(b1)
            print("rowInput executed for row", i)
        else:
            b1.board[i] = [0,0,0,0,0,0,0,0,0]
            print("Empty row added for row", i)

def numberTry(val, m, n):
    cell_attempt_list[m][n].append(val)
    omg.sqr_ref_list[m].remove(val)
    """POssible issue with removing value from ref_list        """

def sudokuSolver(b):#b = b1 or Board()
    #first row
    colIdList = []

    for y in b.board: #starting colID for each row
        n = 0

        if any(isinstance(el, int) for el in y):
            for el in y:
                print(n)
                if el > 0:
                    n += 1
                else:
                    break
            colIdList.append(n)
        else:
            colIdList.append(n)
    # return colIdList

    #remove all future possibilities from constances:
    for i in b.rowList:
            for j in b.sqr[i].posList:
                omg.removeFutureVal(i,j)
                b.cellIdConst(i,j) #setup constants list to ignore using ref list
    print("remove constances future possibilities function executed")
    omg.calcRemainingRefNums()
    print("ref<cnt",omg.ref_cnt)

    # t_cell = omg.FindCellId(rowID, colID - 1)
    start = True
    solverDone = False
    backTrace = False
    cellTracePath = []
    zz = 0

    while solverDone is False and zz < 25:
        zz += 1
        emptyCell = [i for i in range(81) if i not in b.constant]

        priority1 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) < 2]
        priority2 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) == 2]
        priority3 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) >= 3]
        if len(priority1) > 0:
            print("priority 1")
            emptyCellLen = priority1
        elif len(priority2) > 0:

            print("switch to priority2")
            emptyCellLen = priority2[0:1]
            print(emptyCellLen)
            print(emptyCellLen[0], omg.sqr_ref_list[emptyCellLen[0]][0])

            # solverDone = True
            # for j in range(9):
            #     tempColID = []
            #     tempColVal = []
            #     for i in range(j,81,9):
            #         if len(omg.sqr_ref_list[i]) > 0:
            #             for el in omg.sqr_ref_list[i]:
            #                 tempColID.append(i)
            #                 tempColVal.append(el)
            #         else:
            #             pass
            #     print("column",j, "colID", tempColID,"colVal",tempColVal)
            # tempRowID = []
            # tempRowVal = []
            # for j in range(9):
            #     for i in range(j,j+9):
            #         if len(omg.sqr_ref_list[i]) > 0:
            #             for el in omg.sqr_ref_list[i]:
            #                 tempRowID.append(i)
            #                 tempRowVal.append(el)
            #         else:
            #             pass
            #     print("row",j, "rowId", tempRowID,"rowVal",tempRowVal)
        else:
            print("priority 3")
            emptyCellLen = priority3
            solverDone = True


        for i in emptyCellLen:
            try:
                print(i,omg.sqr_ref_list[i])

                row = omg.FindRowFromCell(i)
                col = omg.FindColFromCell(i)
                cellTracePath.append(i)
                b.board[row][col] = omg.sqr_ref_list[i][0]
                omg.removeFutureVal(row,col)
                if any(len(omg.sqr_ref_list[j]) < 1 for j in emptyCellLen if j > i):
                    print("Incorrect move, error", i,omg.sqr_ref_list[i])
                    backTrace = True

                b.cellIdConst(row, col)
            except IndexError:
                for j in b.board:
                    print(j)
        for m in range(9):
            r = boxRow(m)
            c = boxCol(m)
            tempBoxVal = []
            tempBoxID = []
            for i in r:
                for j in c:
                    b_cellID = omg.FindCellId(i, j)
                    if len(omg.sqr_ref_list[b_cellID]) > 0:
                        for el in omg.sqr_ref_list[b_cellID]:
                            tempBoxID.append(b_cellID)
                            tempBoxVal.append(el)
                    else:
                        pass
            for i in range(len(tempBoxVal)):
                if tempBoxVal.count(tempBoxVal[i]) > 1:
                    pass
                else:
                    print("executed box elimination")
                    t_row = omg.FindRowFromCell(tempBoxID[i])
                    t_col = omg.FindColFromCell(tempBoxID[i])
                    b.board[t_row][t_col] = tempBoxVal[i]
                    omg.removeFutureVal(t_row, t_col)
                    b.cellIdConst(t_row, t_col)
        print("ref_list",omg.sqr_ref_list)
        print("constn",b.constant)
        for j in b.board:
            print(j)
        if len(b.constant) == 81:
            solverDone = True
            print(len(b.constant))
        else:
            pass
        # zz += 1
    #     removedPreviousCells = [i for i in emptyCellLen if i > t_cell]
    #     print("removedPreviousCells length", len(removedPreviousCells), "array", removedPreviousCells)
    #     if len(removedPreviousCells) > 0:
    #         if len(ref_list[t_cell]) == 0:
    #             triggerSwitch = True
    #         else:
    #             pass
    #         print("value", row[colID - 1], "removed bc current cell", t_cell, "cause downstream issues")
    #         omg.addFutureVal(rowID, colID - 1, row[colID - 1])
    #         row[colID - 1] = 0
    #         futureCheck = False
    #     else:
    #         futureCheck = True
    #
    # """ solve each row"""
    # for i in range(2):
    #     print("row",b.board[i],"ref_list",omg.sqr_ref_list,"rowID",i,"colID",colIdList[i],"const",b.sqr[i].posList)
    #     b.board[i] = newRowGen(b.board[i],omg.sqr_ref_list,i,colIdList[i],b.sqr[i].posList)
    #     for j in b.board:
    #         print(j)



def boxRow(boxID):
    if any (boxID == i for i in [0,1,2]):
        row = [0,1,2]
    elif any (boxID == i for i in [3,4,5]):
        row = [3, 4, 5]
    else:
        row = [6,7,8]
    return row

def boxCol(boxID):
    if any(boxID ==i for i in [0,3,6]):
        col = [0,1,2]
    elif any(boxID == i for i in [1, 4, 7]):
        col = [3, 4, 5]
    else:
        col = [6, 7, 8]
    return col

def boxUpdate(id,rowID,colID,row):

    if any(rowID == i for i in [0,3,6]):
        if any(colID == i for i in [0, 3, 6]):
            bID = 0
        elif any(colID == i for i in [1, 4, 7]):
            bID = 1
        else:
            bID = 2
    elif any(rowID == i for i in [1,4,7]):
        if any(colID == i for i in [0, 3, 6]):
            bID = 3
        elif any(colID == i for i in [1, 4, 7]):
            bID = 4
        else:
            bID = 5
    else:
        if any(colID == i for i in [0, 3, 6]):
            bID = 6
        elif any(colID == i for i in [1, 4, 7]):
            bID = 7
        else:
            bID = 8

    if id < 3:
        row1 = b1.board[0]
        row2 = b1.board[1]
        row3 = b1.board[2]
    elif id >= 3 and id < 6:
        row1 = b1.board[3]
        row2 = b1.board[4]
        row3 = b1.board[5]
    elif id > 6:
        row1 = b1.board[6]
        row2 = b1.board[7]
        row3 = b1.board[8]
    else:
        print("error")
    rows = [row1, row2, row3]

    # print("length",y_len, "rows", rows)
    temp = []
    if id == 0 or id == 3 or id == 6:
        for j in range(3):
            for ll in range(3):
                temp.append(rows[j][ll])

    elif id == 1 or id == 4 or id == 7:

        for j in range(3):
            for ll in range(3, 6):
                temp.append(rows[j][ll])

    elif id == 2 or id == 5 or id == 8:

        for j in range(3):
            for ll in range(6, 9):
                temp.append(rows[j][ll])

    else:
        pass
    final = temp
    final[bID] = row[colID]
    # print("update function executed current box:", temp, "updated box:", final)

    return final


# y1 = Y(1)
# y2 = Y(2)
# y3 = Y(3)
# y4 = Y(4)
# y5 = Y(4)
# y6 = Y(5)
# y7 = Y(6)
# y8 = Y(7)
# y9 = Y(8)
#
# lst_of_Y = [y1,y2,y3,y4,y5,y6,y7,y8,y9]
#

# y1.rowInput(b1)
# y2.rowInput(b1)
b1 = Board()
initialize_board(b1)
sudoku_input()
# print("y0",b1.board[0])
b1.show()


# print("rowID", 2, "colID",3,"col",colUpdate(b1.board[1],2,2))
sudokuSolver(b1)
# print(b1.board)


# a = newRowGen(b1.board[0],omg.sqr_ref_list,0,0,b1.sqr[0].posList)
# b = newRowGen(b1.board[1],omg.sqr_ref_list,1,0,b1.sqr[1].posList)
# c = newRowGen(b1.board[2],omg.sqr_ref_list,2,0,b1.sqr[2].posList)
# d = newRowGen(b1.board[3],omg.sqr_ref_list,3,0,b1.sqr[3].posList)
# e = newRowGen(b1.board[3],omg.sqr_ref_list,4,0,b1.sqr[4].posList)
# f = newRowGen(b1.board[3],omg.sqr_ref_list,5,0,b1.sqr[5].posList)
# g = newRowGen(b1.board[3],omg.sqr_ref_list,6,0,b1.sqr[6].posList)
# h = newRowGen(b1.board[3],omg.sqr_ref_list,7,0,b1.sqr[7].posList)
# i = newRowGen(b1.board[3],omg.sqr_ref_list,8,0,b1.sqr[8].posList)
# c = newRowGen(y3.row,omg.sqr_ref_list,2,0)
# print("yay:",a)
# print("yay:",b)
# print("yay:",c)
# print("yay:",d)
# print("yay:",e)
# print("yay:",f)
# print("yay:",g)
# print("yay:",h)
# print("yay:",i)