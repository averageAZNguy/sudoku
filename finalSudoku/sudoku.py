import math
import random
from random import shuffle
"""
Initial input classes
"""

# import os
# print(os.path.abspath(os.curdir))
# os.chdir('/home/quang/PycharmProjects/sudoku')
# # print(os.path.abspath(os.curdir))
# f = open( "InputData.txt", "r" )
# lines = []
# for line in f:
#     lines.append(line)
# f.close()
#
# positions = lines[0::2]
# vals1 = lines[1::2]
# positions.pop()
# for i in range(len(positions)):
#     positions[i] = positions[i].replace("\n","")
#     vals1[i] = vals1[i].replace("\n","")


class Board(object):
    rowIDs = []

    def __init__(self, sqr=[],board=[]):
        self.sqr = sqr
        self.rowList = []
        self.constant = []
        self.board = board
        for i in range(9):
            row = []
            for j in range(9):
                row.append([])
            self.board.append(row)
    """prints board"""
    def show(self):
        # row1 = [[],[],[],[],[],[],[],[],[]]

        # print("You have created a new sudoku board")
        for i in self.board:
            print(i)
    """lists the rows user will edit on board 1-9"""
    def rowModify(self):

        inputNum = False
        while inputNum is False:
            r = input("Enter which rows you will add or modify: ")
            r = r.replace(",", "")
            r = r.replace(" ", "")
            for num in r:
                self.rowList.append(int(num) - 1)
            if any(num > 8 or num < 0 for num in self.rowList):
                inputNum = False
                self.rowList = []
                print("Enter number is outside 1-9 bound, re-enter number: ")
            else:
                inputNum = True
    """fill in values and their positions in each of the chosen rows"""
    def initialize(self,rowID,positions,vals):
        temp_row = [0,0,0,0,0,0,0,0,0]
        n = 0
        for pos in positions:
            temp_row[pos] = vals[n]
            n += 1
        self.board[rowID] = temp_row
        self.sqr[rowID].row = temp_row

    def cellIdConst(self,rowID,colID):
        cellID = omg.FindCellId(rowID,colID)
        self.constant.append(cellID)


"""Object rows players will input the values and their positions"""
class Row(object):

    def __init__(self,id):
        # self.row = []
        self.id = id
        self.posList = []
        self.vals = []
        """initiate user to input values,position, etc"""

    def rowInput(self, board):

        rowC = 0
        # position = a list of positions [1-9], an array e.g. [1,3,5]
        modifyComp = False
        while modifyComp is False:
            inputNum = False
            while inputNum is False:
                position = input("Enter the positions for row %s you would like to add/modify the value(1-9: "%(self.id + 1))
                rowC += 1
                temp = position.replace(",","")
                temp = temp.replace(" ","")
                for num in temp:
                    self.posList.append(int(num) -1)
                    if any(num > 8 or num < 0 for num in self.posList):
                        inputNum = False
                        self.poslist = []
                        print("Enter number is outside 1-9 bound, re-enter number")
                    else:
                        inputNum = True
                n = len(self.posList)

                #
                value = input("Enter the numbers you want for these positions %s : " %(self.posList))
                temp = value.replace(",", "")
                temp = temp.replace(" ", "")
                for val in temp:
                    self.vals.append(int(val))
                userComp = input("Are you done?:(Y/N) ")
                if userComp.lower() == "y":
                    modifyComp = True
                else:
                    modifyComp = False

            board.initialize(self.id, self.posList, self.vals)


"""***********************************************"""
"""
Program logic to solve sudoku
"""
"""***********************************************"""

class Col(object):

    def __init__(self,attempt = []):
        self.ref = random.sample(range(1,10),9)





"""
generate a list of empty individual cells. Will be use remove possible values assign to specific cells. WIll be heavilu used in backtrack method
"""
class SqrRefList(object):
    sqr_ref_list = []
    def __init__(self):
        for i in range(81):
            x = []
            for j in range(1, 10):
                x.append(j)
            self.sqr_ref_list.append(x)
        self.ref_cnt = []

    def FindCellId(self,row,col):
        cellId = row*9 + col
        return cellId
    def FindRowFromCell(self,cellID):
        row = int(cellID/9)
        return row
    def FindColFromCell(self,celID):
        col = celID % 9
        return col
    """reduce downstream possibilities"""
    def removeFutureVal(self,rowID,colID):
        # remove all possibilities from row
        startCell = rowID * 9
        val = b1.board[rowID][colID]
        # print("val to be removed",val)
        for k in range(startCell,startCell+9):
            # print("at positions cell", k)
            try:
                self.sqr_ref_list[k].remove(val)
            except ValueError:
                pass

        #remove all possibilities from column
        for k in range(colID,81,9):
            # print("at position cell,",k)
            try:
                self.sqr_ref_list[k].remove(val)
            except ValueError:
                pass
        #remove all possibilities from box
        boxID = box1.findBoxID(rowID,colID)
        #need all the cell id from a box given a boxID
        r = boxRow(boxID)
        c = boxCol(boxID)

        for i in r:
            for j in c:
                try:
                    cellID = self.FindCellId(i, j)
                    # print("from row",i,"& col", j,"at position cell", cellID)
                    self.sqr_ref_list[cellID].remove(val)
                except ValueError:
                    pass
    #return ref list value to each cell
    def addFutureVal(self,rowID,colID,val):
        cellID = self.FindCellId(rowID,colID)
        # add all possibilities to row
        startCell = rowID * 9
        # print("val to be added", val)
        for k in range(startCell, startCell + 9):
            # print("at positions cell", k)
            if any(i == val for i in self.sqr_ref_list[cellID]):
                pass
            else:
                self.sqr_ref_list[cellID].append(val)

        # add all possibilities to col
        for k in range(colID, 81, 9):
            # print("at position cell,", k)
            if any(i == val for i in self.sqr_ref_list[cellID]):
                pass
            else:
                self.sqr_ref_list[cellID].append(val)

        # add all possibilities to box
        boxID = box1.findBoxID(rowID, colID)
        # need all the cell id from a box given a boxID
        r = boxRow(boxID)
        c = boxCol(boxID)

        for i in r:
            for j in c:
                b_cellID = self.FindCellId(i, j)
                if any(i == val for i in self.sqr_ref_list[b_cellID]):
                    pass
                else:
                    print("from row", i, "& col", j, "at position cell", b_cellID)
                    self.sqr_ref_list[b_cellID].append(val)

    def calcRemainingRefNums(self):
        temp = []
        for i in self.sqr_ref_list:
            if len(i) < 1:
                return
            temp.append(len(i))
        self.ref_cnt = temp

class LastSavedCellId(object):

    def __init__(self,):
        self.cellID = []
        self.numberTry = []


LSCID = LastSavedCellId()
omg = SqrRefList()
cell_attempt_list = []
for i in range(81):
    x = []
    for j in range(1,10):
        x.append([])
    cell_attempt_list.append(x)

"""
3x3 constraint reference list
"""
class Boxes(object):
    def __init__(self):
        self.sqr = [[],[],[],[],[],[],[],[],[]] #used to append numbers as they're added to check for duplicates
        self.check = [[],[],[],[],[],[],[],[],[]] #used to determine if everything is passing: box is either "Good" or "bad"

    def findBoxID(self,row,m):
        if row < 3 and m < 3:
            return 0
        elif row < 3 and m >= 3 and m < 6:
            return 1
        elif row <3 and m >= 6:
            return 2
        elif row >= 3 and row < 6 and m < 3:
            return 3
        elif row >= 3 and row < 6 and m >= 3 and m < 6:
            return 4
        elif row >= 3 and row < 6 and m >= 6:
            return 5
        elif row >= 6 and row < 9 and m < 3:
            return 6
        elif row >= 6 and row < 9 and m >= 3 and m < 6:
            return 7
        elif row >= 6 and row < 9 and m >= 6:
            return 8
        else:
            return 10
    # Returns 3x3 box for cell that has been added with number
    def update(self,id):
        if id < 3:
            row1 = b1.board[0]
            row2 = b1.board[1]
            row3 = b1.board[2]
        elif id >= 3 and id < 6:
            row1 = b1.board[3]
            row2 = b1.board[4]
            row3 = b1.board[5]
        elif id > 6:
            row1 = b1.board[6]
            row2 = b1.board[7]
            row3 = b1.board[8]
        else:
            print("error")
        rows = [row1,row2,row3]

        # print("length",y_len, "rows", rows)
        temp = []
        if id == 0 or id == 3 or id == 6:
            for j in range(3):
                for ll in range(3):
                    temp.append(rows[j][ll])

        elif id == 1 or id == 4 or id == 7:

            for j in range(3):
                for ll in range(3,6):
                    temp.append(rows[j][ll])

        elif id == 2 or id == 5 or id == 8:

            for j in range(3):
                for ll in range(6,9):
                    print("ll",ll)
                    temp.append(rows[j][ll])

                print(temp)
        else:
            pass
        # print("update function executed current box:",self.sqr[id], "updated box:", temp)

        return temp


box1 = Boxes()




def colUpdate(row,rowID,colID):
    cellVal = row[colID]
    tempCol = []
    for i in range(9):
        if i == rowID:
            tempCol.append(cellVal)
        else:
            tempCol.append(b1.board[i][colID])
    return tempCol







"""***********************************************"""

""" Generate Empty Board """

def initialize_board(board):

    y0 = Row(0)
    y1 = Row(1)
    y2 = Row(2)
    y3 = Row(3)
    y4 = Row(4)
    y5 = Row(5)
    y6 = Row(6)
    y7 = Row(7)
    y8 = Row(8)
    board.sqr = [y0,y1,y2,y3,y4,y5,y6,y7,y8]


""" User input Row, position, and assign value to each cell"""
def sudoku_input():

    b1.show()
    b1.rowModify()
    print("rowList", b1.rowList)
    rows = b1.rowList
    for i in range(9):
        if i in rows:
            b1.sqr[i].rowInput(b1)
            print("rowInput executed for row", i)
        else:
            b1.board[i] = [0,0,0,0,0,0,0,0,0]
            print("Empty row added for row", i)
"""Removes value from reference list once its been used"""
def numberTry(val, m, n):
    cell_attempt_list[m][n].append(val)
    omg.sqr_ref_list[m].remove(val)

""" Main logic for solver """
def sudokuSolver(b):#b = b1 or Board()
    #first row
    colIdList = []

    for y in b.board: #starting colID for each row
        n = 0

        if any(isinstance(el, int) for el in y):
            for el in y:
                print(n)
                if el > 0:
                    n += 1
                else:
                    break
            colIdList.append(n)
        else:
            colIdList.append(n)
    # return colIdList

    #remove all future possibilities from constances:
    for i in b.rowList:
            for j in b.sqr[i].posList:
                omg.removeFutureVal(i,j)
                b.cellIdConst(i,j) #setup constants list to ignore using ref list
    print("remove constances future possibilities function executed")
    omg.calcRemainingRefNums()
    print("ref<cnt",omg.ref_cnt)

    # t_cell = omg.FindCellId(rowID, colID - 1)
    start = True
    solverDone = False
    backTrace = False
    savedTryCellIDs = {}
    savedTryCellKeyOrder = []
    zz = 0

    while solverDone is False and zz < 25:
        zz += 1
        emptyCell = [i for i in range(81) if i not in b.constant]

        priority1 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) < 2]
        priority2 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) == 2]
        priority3 = [i for i in emptyCell if len(omg.sqr_ref_list[i]) >= 3]
        if len(priority1) > 0:
            print("priority 1")
            emptyCellLen = priority1
            priority = 1
        elif len(priority2) > 0:

            print("switch to priority2")
            emptyCellLen = priority2[0:1]
            print(emptyCellLen)
            print(emptyCellLen[0], omg.sqr_ref_list[emptyCellLen[0]][0])

            LSCID.cellID.append(emptyCellLen[0])
            LSCID.numberTry.append(omg.sqr_ref_list[emptyCellLen[0]])
            priority = 2
            print("saved cell ID", savedTryCellIDs)
        else:
            print("priority 3")
            emptyCellLen = priority3
            solverDone = True


        for i in emptyCellLen:
            try:
                print(i,omg.sqr_ref_list[i])

                row = omg.FindRowFromCell(i)
                col = omg.FindColFromCell(i)
                b.board[row][col] = omg.sqr_ref_list[i][0]
                omg.removeFutureVal(row,col)
                b.cellIdConst(row, col)
                ref = [k for k in range(81) if k not in b.constant]
                if any(len(omg.sqr_ref_list[j]) < 1 for j in ref if j != i):
                # if any(len(omg.sqr_ref_list[j]) < 1 for j in emptyCellLen if j > i):
                    print("Incorrect move, error", i,omg.sqr_ref_list[i])
                    print("caused by ref", [m for m in ref if len(omg.sqr_ref_list[m]) < 1])
                    backTrace = True
                    # break

            except IndexError:
                print("Index Error for cell", i)
                for j in b.board:
                    print(j)
        for m in range(9):
            r = boxRow(m)
            c = boxCol(m)
            tempBoxVal = []
            tempBoxID = []
            for i in r:
                for j in c:
                    b_cellID = omg.FindCellId(i, j)
                    if len(omg.sqr_ref_list[b_cellID]) > 0:
                        for el in omg.sqr_ref_list[b_cellID]:
                            tempBoxID.append(b_cellID)
                            tempBoxVal.append(el)
                    else:
                        pass
            for i in range(len(tempBoxVal)):
                if tempBoxVal.count(tempBoxVal[i]) > 1:
                    pass
                else:
                    print("executed box elimination")
                    t_row = omg.FindRowFromCell(tempBoxID[i])
                    t_col = omg.FindColFromCell(tempBoxID[i])
                    b.board[t_row][t_col] = tempBoxVal[i]
                    omg.removeFutureVal(t_row, t_col)
                    b.cellIdConst(t_row, t_col)
        print("ref_list",omg.sqr_ref_list)
        print("constn",b.constant)
        for j in b.board:
            print(j)
        if len(b.constant) == 81:
            solverDone = True
            print(len(b.constant))
        else:
            pass

        if backTrace is True:
            # backTrace(savedTryCellIDs,savedTryCellKeyOrder)
            print("Trigger backtracing program")
        else:
            pass
    final = 0
    for i in omg.sqr_ref_list:
        for j in i:
            final += j
    if len(b.constant) == 81 and final == 0:
        print("Sudoku board is solvable")
    else:
        print("Sudoku is not solvable")

def backTrace(dic, keyOrder):
    lastSavedCellID = LSCID.cellID[len(LSCID.cellID)-1]

    currCellID = b1.constant[len(b1.constant)-1]
    sameCellID = currCellID == lastSavedCellID

    n = 0
    """ Erase Previous attempts until it reachs last saved point"""
    while sameCellID is False:
        returnCellID = b1.constant.pop()
        rRow = omg.FindRowFromCell(returnCellID)
        rCol = omg.FindColFromCell(returnCellID)
        rVal = b1.board[rRow][rCol]
        omg.addFutureVal(rRow,rCol,rVal)
        currCellID = b1.constant[len(b1.constant) - 1]
        sameCellID = currCellID == lastSavedCellID
        n += 1
        if n > 100:
            print("Error Loop nested in sameCellID loop. CellID:", currCellID)
            break
        else:
            pass
    else:
        returnCellID = b1.constant.pop()
        rRow = omg.FindRowFromCell(returnCellID)
        rCol = omg.FindColFromCell(returnCellID)
        rVal = b1.board[rRow][rCol]
        omg.addFutureVal(rRow, rCol, rVal)

    altVal = LSCID.numberTry[len(LSCID.cellID)-1]
    if len(altVal) > 0:
        omg.sqr_ref_list[lastSavedCellID].remove(altVal[0])
        # LSCID.numberTry[len(LSCID.cellID)-1].remove(altVal[0])
    else:
        keyOrder.remove(lastSavedCellID)
        print("Removing", altVal[0], "from cell",lastSavedCellID)
        omg.sqr_ref_list[lastSavedCellID].remove(altVal[0])



def boxRow(boxID):
    if any (boxID == i for i in [0,1,2]):
        row = [0,1,2]
    elif any (boxID == i for i in [3,4,5]):
        row = [3, 4, 5]
    else:
        row = [6,7,8]
    return row

def boxCol(boxID):
    if any(boxID ==i for i in [0,3,6]):
        col = [0,1,2]
    elif any(boxID == i for i in [1, 4, 7]):
        col = [3, 4, 5]
    else:
        col = [6, 7, 8]
    return col

def boxUpdate(id,rowID,colID,row):

    if any(rowID == i for i in [0,3,6]):
        if any(colID == i for i in [0, 3, 6]):
            bID = 0
        elif any(colID == i for i in [1, 4, 7]):
            bID = 1
        else:
            bID = 2
    elif any(rowID == i for i in [1,4,7]):
        if any(colID == i for i in [0, 3, 6]):
            bID = 3
        elif any(colID == i for i in [1, 4, 7]):
            bID = 4
        else:
            bID = 5
    else:
        if any(colID == i for i in [0, 3, 6]):
            bID = 6
        elif any(colID == i for i in [1, 4, 7]):
            bID = 7
        else:
            bID = 8

    if id < 3:
        row1 = b1.board[0]
        row2 = b1.board[1]
        row3 = b1.board[2]
    elif id >= 3 and id < 6:
        row1 = b1.board[3]
        row2 = b1.board[4]
        row3 = b1.board[5]
    elif id > 6:
        row1 = b1.board[6]
        row2 = b1.board[7]
        row3 = b1.board[8]
    else:
        print("error")
    rows = [row1, row2, row3]

    # print("length",y_len, "rows", rows)
    temp = []
    if id == 0 or id == 3 or id == 6:
        for j in range(3):
            for ll in range(3):
                temp.append(rows[j][ll])

    elif id == 1 or id == 4 or id == 7:

        for j in range(3):
            for ll in range(3, 6):
                temp.append(rows[j][ll])

    elif id == 2 or id == 5 or id == 8:

        for j in range(3):
            for ll in range(6, 9):
                temp.append(rows[j][ll])

    else:
        pass
    final = temp
    final[bID] = row[colID]
    # print("update function executed current box:", temp, "updated box:", final)

    return final


""" Initialize solver """

b1 = Board()
initialize_board(b1)
sudoku_input()
b1.show()
sudokuSolver(b1)



